let residents = [];

function getIDArray(residents_array) {
    let IDArray = [];
    residents_array.forEach((item) => {
        let args = item.split("/");
        IDArray.push(args[args.length - 1]);
    });
    return IDArray;
}

function loadResidents(location_id) {
    let url = "https://rickandmortyapi.com/api/location/" + location_id;
    let request = new Request(url, {method: 'GET',});

    fetch(request)
        .then(response => response.ok ? response.json() : Response.error())
        .then(json => {
            total_residents = json["residents"].length;
            loadCharacters(getIDArray(json["residents"]));
        })
        .catch(() => {
            return null;
        })
}

function loadCharacters(characters_list) {
    let url = "https://rickandmortyapi.com/api/character/" + characters_list.join(",");
    let request = new Request(url, {method: 'GET',});

    fetch(request)
        .then(response => response.ok ? response.json() : Response.error())
        .then(json => {
            residents = json;
            displayResidents(residents);
        })
        .catch(() => {
            return null;
        })
}

function countSize(display_width, display_height) {
    let max_square = 0;
    let size = 10;

    for (let i = 10; i < 1000; i++) {
        let w = Math.floor(display_width / i);
        let h = Math.floor(display_height / i);

        if (w * h >= (total_residents + 1) && i * i * (total_residents + 1) > max_square) {
            max_square = i * i * (total_residents + 1);
            size = i;
        }

    }

    return size;
}

function displayResidents(residents_list) {
    let x = 0;
    let y = 0;
    let display_width = document.documentElement.clientWidth;
    let display_height = document.documentElement.clientHeight;
    let side = countSize(display_width, display_height);

    document.getElementById('container').setAttribute("viewBox", "0 0 " + display_width + " " + display_height);
    document.getElementById("container").appendChild(renderResident(x, y, side, null));
    x += side;
    if (x + side > display_width) {
        x = 0;
        y += side;
    }

    residents_list.forEach(item => {
        let resident = renderResident(x, y, side, item);
        document.getElementById("container").appendChild(resident);

        x += side;
        if (x + side > display_width) {
            x = 0;
            y += side;
        }
    });

}

function renderResidentInfo(x, y, side, character) {
    let popup = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    popup.setAttribute("class", "resident-popup");

    let back = renderRect(x, y, side, side);
    back.setAttribute("fill", "rgb(200, 100, 100)");

    popup.appendChild(back);
    popup.appendChild(renderText(x + 2, y, 15, character["name"]));
    popup.appendChild(renderText(x + 2, y + 17, 15, "status : " + character["status"]));
    popup.appendChild(renderText(x + 2, y + 34, 15, "species : " + character["species"]));
    popup.appendChild(renderText(x + 2, y + 51, 15, "type : " + character["type"]));
    popup.appendChild(renderText(x + 2, y + 68, 15, "gender : " + character["gender"]));

    return popup;
}

function renderResident(x, y, side, character = null) {
    let resident;
    if (character !== null) {
        let image = renderImage(x, y, side, side, character["image"]);
        let popup = renderResidentInfo(x, y, side, character);

        resident = document.createElementNS("http://www.w3.org/2000/svg", 'g');
        resident.setAttribute("class", "resident");
        resident.appendChild(image);
        resident.appendChild(popup);
    }
    else {
        resident = renderLinkWrapper("index.html");

        let back = renderRect(x, y, side, side);
        back.setAttribute("fill", "rgb(200, 100, 100)");

        let wrapper = document.createElementNS("http://www.w3.org/2000/svg", 'g');
        wrapper.appendChild(back);
        wrapper.appendChild(renderText(x, y + 35, 35, "back"));

        resident.appendChild(wrapper);
    }

    return resident;
}
