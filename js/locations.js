let locations = [];
let total_residents = 0;

function loadAllLocations(url = 'https://rickandmortyapi.com/api/location/') {

    let request = new Request(url, {method: 'GET',});

    fetch(request)
        .then(response => response.ok ? response.json() : Response.error())
        .then(json => {
            locations = locations.concat(json["results"]);
            json["results"].forEach(
                item => total_residents += (item["residents"].length + 1)
            );

            if (json["info"]["next"] !== "")
                loadAllLocations(json["info"]["next"]);
            else
                displayLocations();

        })
        .catch(() => {
            return null;
        })
}

function displayLocations() {
    locations.sort((a, b) => {
        return b["residents"].length - a["residents"].length
    });
    document.getElementById('container')
        .setAttribute("viewBox", "0 0 " + total_residents + " " + total_residents);
    let margin_y = total_residents;

    locations.forEach((data) => {
        margin_y -= (data["residents"].length + 1);
        let location = renderLocation(margin_y, data);
        let link = renderLinkWrapper("index.html?location_id=" + data["id"]);
        document.getElementById('container').appendChild(link).appendChild(location);
    });
}

function getRandomColor() {
    let r = Math.round(Math.random() * 255);
    let g = Math.round(Math.random() * 255);
    let b = Math.round(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
}

function renderLocation(y, data) {
    let location = renderRect(0, y, total_residents, (data["residents"].length + 1));
    location.setAttribute("id", data["id"]);
    location.setAttribute("class", "location");
    location.setAttribute("fill", getRandomColor());

    let popup = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    popup.setAttribute("class", "popup");

    let popup_back = renderRect(0, y, 150, 50);
    popup_back.setAttribute("fill", "rgb(200, 100, 100)");

    popup.appendChild(popup_back);
    popup.appendChild(renderText(2, y + 10, 6, "Name : " + data["name"]));
    popup.appendChild(renderText(2, y + 2, 6, "Residents : " + data["residents"].length));
    popup.appendChild(renderText(2, y + 18, 6, "Type : " + data["type"]));
    popup.appendChild(renderText(2, y + 26, 6, "Dimension : " + data["dimension"]));

    let group = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    group.appendChild(location);
    group.appendChild(popup);

    return group;
}

function clearContainer(id) {
    let container = document.getElementById(id);
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
}
