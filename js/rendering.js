function renderRect(x, y, width, height) {
    let rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    rect.setAttribute("x", x);
    rect.setAttribute("y", y);
    rect.setAttribute("width", width);
    rect.setAttribute("height", height);
    return rect;
}

function renderText(x, y, size, content) {
    let text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    text.setAttribute("x", x);
    text.setAttribute("y", y + size);
    text.setAttribute("font-size", size);
    text.setAttribute("font-family", "Verdana");
    text.setAttribute("fill", "blue");
    text.textContent = content;
    return text;
}

function renderLinkWrapper(href) {
    let link = document.createElementNS("http://www.w3.org/2000/svg", 'a');
    link.setAttribute("href", href);
    link.setAttribute("style", "cursor: pointer");
    return link;
}

function renderImage(x, y, width, height, href) {
    let image = document.createElementNS("http://www.w3.org/2000/svg", 'image');
    image.setAttribute("href", href);
    image.setAttribute("x", x);
    image.setAttribute("y", y);
    image.setAttribute("width", width);
    image.setAttribute("height", height);
    return image;
}